NOTE: App will receive a flag in Business Search APIs - **"isSubscribed"**: true/false.   
Depending upon the this flag, user will see icon  if true:= "Following" otherwise if false:="Follow"    
   
   if User clicks of "Follow": he will be presented with confrimation box "I agree to receive notification from <business Name>" - Yes/No.    
   If user clicks "Yes": trigger this API. Clicks "No": simply close dialog.    
   
Description : User with id=userId will be subscribed to business having id= businessId. If a user has already subscribed, API will simply return true. 

URL - **/api/broadcast/subscribe**

Method - POST

Fields:    

```
#!python

1. userId: ajklasdfas5df64asd5asd (valid individual id)
2. businessId - 123sad1154qw613as13aadas (Valid Business ID)    

```    
    
resposne:    
1. *success*      
{    
  "success": true,    
  "error": null,    
  "data": null   
}   
    
2. Invalid Ids    
{   
  "success": false,    
  "error": {   
    "msg": "Error: Invalid subscriber/user id"    
  },   
  "data": null    
}    