# Publish a Broadcast Message (as a Business) to active (individual) subscribers    
    
      
Use this API to POST boradcast message to Active Subscribers from "Broadcast a message" screen.    
use allowed message type only i.e TEXT/IMAGE/AUDIO etc ...    
**POST**
```
#!python
/api/broadcast/broadcastMessage        
```
    
**Post Params** - all 4 params are **required**     
```
#!python
1. businessId = 570fe836d1a4a25c3d8a2ca4   
2. messageType = TEXT/IMAGE/PDF/DOC/AUDIO/CONTACT only       
3. mediaURL = 's3_urlxxx.com' if its non-text message/ for text message keep this blank    
4. message = "message if type is text message"/ for multimedia message - keep this field blank   
```
    
Response    
1. **success**
{    
  "success": true,    
  "error": null,    
  "data": {    
    "msg": "Message broadcasted successfully",    
    "broadcastId": "57a38724710455912a56c57a"    
  }    
}      
    
2. **Error:**    
{   
  "success": false,    
  "error": {    
    "msg": "Error: Unsupported Broadcast Message Type."    
  },      
  "data": null    
}     