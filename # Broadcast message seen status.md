# Mark broadcast message as read when individual sees the broadcast message.    
    
      
Use this API to mark boradcast message as seen by particular individual.    

**POST**
```
#!python
/api/broadcast/markAsSeen        
```
    
**Post Params** - all 2 params are **required**     
```
#!python
1. broadcastId = 570fe836d1a4a25c3d8a2ca4   
2. userId = 570fe836d1a4a25c3d8a2ca4        
```
    
Response    
1. **success**
{
    "success": true,
    "error": null,
    "data": null
}      
    
2. **Error:**    
{
  "success": false,
  "error": {
    "msg": "Error: User not found"
  },
  "data": null
}    