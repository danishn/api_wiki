URL - /api/business/register    
    
Business DTO - Its a Multipart Request    

```
#!python

{    
"business" :    
 {   
 "id": null,    
 "name":"Bhushan Technologies",    
 "address" : "Kothrud, Pune",     
 "contactNo" : 9876543210,             
 "description": "blah! blah! blah!",    
 "country" : "India",    
 "city" : "Pune",     
 "categories" : [],         
 "email" : "user@conchact.com",      
 "password" : "aaaaaa",    
 "lat":"18.572316",
 "lng":"57.123456",    
 "gcmId": "gcm123string"/"null"   
 "apnId": "apn123string"/"null"   
 "showContact" = true/false (checkbox in UI)    
 },    
       
 "profilePicture": "[IMAGE]"     
    
}  
    
     
----------------------------------------------------------
Updated    
{    
"business" :      
{
    "name": "Abcd",
    "address": "Abcd",
    "contactNo": "214536987",
    "description": "Abcd",
    "country": {
        "callingCode": "91",
        "countryCode": "IN",
        "id": null,
        "name": "India"
    },
    "city": {
        "country": " India",
        "id": "55f52c5e7a42987b4e90968f",
        "name": "Pune",
        "placeId": "ChIJARFGZy6_wjsRQ-Oenb9DjYI"
    },
    "categories": [
        {
            "id": null,
            "name": "Deals & Promotions",
            "selected": true
        }
    ],
    "email": "at@best.com",
    "password": "123456",
    "gcmId": "APA91bGSVwM9U_dAPK7OQyPIVWBw_HS1KpyEH4TrvKp-el5_V-FS3YZz9uLDO",
    "apnId": "",
    "lat": "18.5903728",
    "lng": "73.8185433",    
    "showContact" = true/false (checkbox in UI)
}  
}    
```
