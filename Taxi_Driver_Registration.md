URL - /api/taxi/register    
    
Taxi Driver - Its a Multipart Request    

```
#!python

     
   {taxi: 
    { 
     "id":null, // If "Individual" already registered is trying to register as a Taxi Driver, then send Individual's Id here,  
     "name":"test taxidriver",    
     "contactNo":9876543210,    
     "email":"ii@gmail.com",      
     "password":"aaaaaa",    
     "gcmId":"ashdg783as6ddsan873n98asasdas45as4d4a31fa8asda4",      
     "apnId":"ashdg783as6ddsan873n98asasdas45as4d4a31fa",
     "carModel":"dsddd12",    
     "carRegistrationNo":"765769587965",     
     "drivingLicenceNo":"uyyuyuit",   
     "country":countryObject,     
     "city":cityObject,      
     "lat":"18.524612",      
     "lng":"72.124563"     
   }},
   
"profilePicture": "[IMAGE]"    
```
