URL - /api/rateBusiness

Method - POST

Fields:    

```
#!python

businessId - 123sad1154qw613as13aadas (Valid Business ID)    
rate - 3.5 (float 0 to 5)     
Individual userId  - as43544ds32fs1ad1cx23ds (Valid User ID)

```

Description : Rates an existing business. If a user has already rated, then it will overwrite the previous one. 