# Get broadcast message analytics for given broadcast message    
    
      
Use this API to fetch & show broadcast analytics business user.     
1. Analytics: How many Users received/viewed the broadcast message with their names    
2. Analytics by receivers' cities    
3. Delivery status of each user    
```
#!python
/api/broadcast/stat?broadcastId=5797b202c5c7f77e49851683        
```
    
Response    
1. **success**
{
    "success": true,
    "error": null,
    "data": {
        "sent": 2,
        "delivered": 0,
        "users": [
            {
                "userId": "565e9c65102c197f2ffa7e14",
                "isDelivered": false,
                "deliveredOn": null,
                "name": "Biswajit Bardhan",
                "email": "sunshine.cst.07@gmail.com",
                "contactNo": "8149629644",
                "profilePicture": "",
                "countryName": "India",
                "cityName": "Pune",
                "showContact": true
            },
            {
                "userId": "578294dd44cd027220c9f7f8",
                "isDelivered": false,
                "deliveredOn": null,
                "name": "Danish",
                "email": "danishnadaf+2@gmail.com",
                "contactNo": "8793700938",
                "profilePicture": "",
                "countryName": "India",
                "cityName": "Pune",
                "showContact": true
            }
        ]
    }
}   
    
2. **Error:**    
{
    "success": false,
    "error": {
        "msg": "Error: No Broadcast Message found"
    },
    "data": null
}  