# Get broadcast history for business User    
    
      
Use this API to fetch & show history of broadcast messages for logged in business user.     
```
#!python
/api/broadcast/history?businessId=570fe836d1a4a25c3d8a2ca4        
```
    
Response    
1. **success**
{    
  "success": true,    
  "error": null,    
  "data": [   
    {    
      "_id": "579a5c9c2075329169bd328c",    
      "sentOn": "2016-07-28T19:27:24.949Z",   
      "messageType": "TEXT",    
      "mediaURL": null,    
      "message": "test message",   
      "count": 1    
    }    
    ]       
}    
    
2. **Error:**    
{
    "success": false,
    "error": {
        "msg": "Error: Business not found"
    },
    "data": null
}   