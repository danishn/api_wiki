```
#!javascript
URL - /api/business/searchByCategoryCityBusiness?catdesc=real&q=in&city=pune&pageNo=2&lat=18.15426&lng=72.4561235&userId=562fb470b10600e71346caa8

```



**Method** - GET

**Pagination** - Yes

Search businesses with a particular '**city**' name that matches the '**catdesc**' value with the category name or description field and a query string '**q**' that matches with the business title field.

    
--
'**city**' : by default user's city.   
'**catdesc**' : the Category name or category Description field   
'**q**' : the query string  "business title" field.    
'**pageNo**' : requested page number. Should not exceed total pages sent in response    
'**lat**' : location latitude for requesting user      
'**lng**' : location longitude for requesting user    
'**userId**' : user's - individual id     


**Response**   

Along with business objects, it returns 

**totalRecords** : Total number of records returned.

**pageSize** : No. of records in current page.

**totalPages** : Total number of pages

**currentPate**" : Current page number.



**Note** - *If the page# in request parameter exceeds the actual number of pages, it will return the result of 1st page.*