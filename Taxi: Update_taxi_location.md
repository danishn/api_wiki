URL - 
```
#!python

/api/taxi/updateLocation
```

   
Method - POST    
    

```
#!python

Header Params (Required)     
userid: taxi user id   
```

    
POST Params (Required)   
```
#!python

lat: taxi location - latitude     
lng: taxi location - longitude 
```
    
    
Update taxi location on server in every 10 sec (time may change later as per business requirement)   