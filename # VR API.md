
```
#!python

URL: /api/vr/list
```

Note: we will have a "Virtual Reality" category in Category list (returned by **Category API**).   
   
As User clicks on this VR icon - on home screen, app will open a new screen and fetch data from above API.   
Above API will return list of VR links along with Title, views and created date.    
    

```
#!python
{
        "_id": "570a3600a13fed152bbe12cc",
        "title": "Swimming Pool",
        "imgUrl": "imageURL",
        "externalUrl": "www.digikore.com/vr/apps/sp/swimming-pool.html",
        "createdOn": "Sun Apr 10 2016",
        "views": 500
}
```
    
render above information in list view.    

    
**On click of any VR link - app will open that link inside a webView - within the app**