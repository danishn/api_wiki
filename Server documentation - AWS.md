*This document will help you to start smooth development for Conchact server side node development*

### **AWS instance workspace Details:**   
    
IP: 54.169.74.32    
    
Biswajit: Developer-1 : /home/ubuntu/conchact_develop
    
Rohit: Developer-2 : /home/ubuntu/conchact_ui

### **Git branching Details:** 
    
* **Whenever starting with new feature:**    
1. checkout and take latest pull from develop branch into your workspace i.e ***git checkout develop and then git pull origin develop***   
2. create a new feature branch with proper name i.e ***git checkout feature/<newBranchName>*** ex. feature/taxi    
3. develop the feature and commit to your branch i.e feature/<newBranchName>    
4. Go to *[Pull Request](https://bitbucket.org/conchact/conchact_server/pull-requests/)* section and create pull request from your branch to develop     
     

* **Development branch : *develop* **     
1. This branch is available to test our development and will be merged to **PUSH** branch, once tested.     
      
* **Live branch : *push* **  
1. **Never** commit on this branch.    
2. This branch is deployed on live server.       
3. Commits on this branch will be **only through pull requests**.     
       

### **Forever** ###
     
 forever is used to run node server forever till you dont stop it.     
Reference docs: https://www.npmjs.com/package/forever

Mostly we will need:    
1. forever list : list of all node server instances running on your AWS instance.    
2. forever start app.js : will start your app.js as a server instance at particular port (9090 or 9000 etc). this port is configured in app/js file of workspace root.    
3. forever stop <index> .e forever stop 0 : this will stop server instance started by forever at index 0.    
   
**Note** : please verify our instance index using list command before stopping the instance.    
     
4. You can find all logs made by your node instance at log file created by forever.    
Just list the instance -> choose your instance -> nano <your log file path>    
    
### **Database interface** ###
DB used here is mongodb.   
- db interface runs at 8081 port. i.e: http://54.169.74.32:8081/    
- if you create any new collection or db (not a new row)-> just restart this interface to see changes.    
- to restart : go to -> node_modules/mongo-express and run app.js file using forever command. i.e forever app.js    
- Note: this file will also be listed in *forever list* command.    
    
Have Fun.. :)
    