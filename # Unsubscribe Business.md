**NOTE**: if business falg "isSubscribed" is set as true, user will see icon of following (with green check-mark).    
And if user clicks on same icon again (to unfollow), they will be presented by an confirmation "Are you sure to unfollow <business name>?" Yes/No   
If user click "Yes": trigger this API to unsubscribe. And icon will turn to follow (with white check-mark)    

Description : User with id=userId will be unsubscribed to business having id= businessId. If a user has already subscribed, API will simply return true. 

URL - **/api/broadcast/unsubscribe**

Method - POST

Fields:    

```
#!python

1. userId: ajklasdfas5df64asd5asd (valid individual id)
2. businessId - 123sad1154qw613as13aadas (Valid Business ID)    

```    
    
resposne:    
1. *success*      
{    
  "success": true,    
  "error": null,    
  "data": null   
}   
    
2. Invalid Ids    
{   
  "success": false,    
  "error": {   
    "msg": "Error: Invalid subscriber/user id"    
  },   
  "data": null    
}    