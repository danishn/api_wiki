# Get list of active subscribers & subscriber count    
    
      
Use this API to show count of **Active Subscribers** on the "Broadcast a message" scree.
```
#!python
/api/broadcast/getSubscribers?businessId=562fc903b10600e71346cab7        
```
    
Response    
1. **success**
{    
  "success": true,    
  "error": null,    
  "data": {    
    "subscriberCount": 1,    
    "subscriberList": [    
      {    
        "userId": "562f2c1ab10600e71346c8c6",    
        "subscribedOn": "2016-07-20T18:16:16.279Z"    
      }    
    ]    
  }    
}    
    
2. **Error:**    
{    
  "success": false,    
  "error": {    
    "msg": "Error: Business not found"    
  },    
  "data": null    
}    