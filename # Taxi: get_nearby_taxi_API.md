URL -     
    

**1.** *GET all taxis:* active+inactive, within specified *range* (i.e radius in km)
```
#!python

/api/taxi/getTaxis?lat=18.580643&lng=73.820813&range=7
```

   
**2.** *GET nearby active taxis:* (dont specify range, it will return 5 taxis within range of 5km)
```
#!python

 /api/taxi/getTaxis?lat=18.580643&lng=73.820813
```

Method - GET    
    

```
#!python

Header Params (Required)     
userid: app user's id   
```

    
Get Params (Required)   
```
#!python

lat: user's location - latitude     
lng: user's location - longitude 
range(optional): radius of area visible on map. 
```
    
    
**Scene 1**: call this API initially when user comes in Taxi section and get all taxis and plot marker (or Clusters) on map. This data will not contain taxi's actual distance and travel time.   
As user zoom out/in on screen, modify range param as required and call API again   
this is Just to show registered taxis on Conchact. and may contain inactive taxis as well    
     
**Scene 2**: call this API **without** *range* param when user clicks on "*Get me a taxi*" button on screen, it will by default filter result and return 5 active taxis within range of 5km. Plot markers for this taxis and open Taxi details POPUP on click of any of these marker.    
this is to get actual active taxis with travel time and travel distance.
