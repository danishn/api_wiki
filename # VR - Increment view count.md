
```
#!python

URL: /api/vr/view
```
**Method: POST**
   
DATA -    
**vrId = 570a9c42b37bd5eb2da5cfc5**    
**userId = 120a9c3256asdgh546b37bd5eb2**    
   
NOTE- app will trigger this API to increament view count of server for VR links.    
when user clicks on any VR link and open in app, view count will be incremented on app side locally and also hit this API to notify server.