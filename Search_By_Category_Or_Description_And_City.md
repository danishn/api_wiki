```
#!javascript
URL - /api/business/searchByCategoryCity?q=in&city=pune&pageNo=1&lat=18.15426&lng=72.4561235&userId=562fb470b10600e71346caa8

```



**Method** - GET

**Pagination** - Yes

Search businesses with a particular   
'**city**' : by default user's city.   
'**q**' : the query string  with the Category name or category Description field.    
'**pageNo**' : requested page number. Should not exceed total pages sent in response       
'**lat**' : location latitude for requesting user       
'**lng**' : location longitude for requesting user      
'**userId**' : user's - individual id    



**Response**   
Along with business objects, it returns 

**totalRecords** : Total number of records returned.

**pageSize** : No. of records in current page.

**totalPages** : Total number of pages

**currentPate**" : Current page number. 



**Note** - *If the page# in request parameter exceeds the actual number of pages (or <= 0), it will return the result of 1st page.*