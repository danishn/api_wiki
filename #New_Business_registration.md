# New Business Registration process #

We have imported around 3Lac businesses from some external data source. These business are available in database but they are not *registered* in Conchact Server (i.e They do not have chat capabilities).      
Whenever any User sends chat message to such business, server sends a **text SMS** to business on registered mobile  Number. And also sends a Invite message to tell business to install Conchact and get registered.     
      
To allow such business to get registered on Conchact app, we will be re-designing our Business Registration Process.    
This process will be divided into two steps    
1. *Check mobile Number availability/eligibility*    
2. *Actual Registration*    
     

### Step 1 ###
**Check Mobile number availability**    
On registration form, mobile number field will be first field that user has to enter. All other fields will be disabled till then.

API - GET
```
#!python

URL /api/checkMobileNo?mobile=8793700938
```

**Response:**

```
#!python

{
  "success": true,
  "error": {},
  "data": {
    "business": {}
    "individual": {}
    "eligible": true
  }
}
```

**Case 1: "eligible": true** - autofill registration form with available info and enable other fields.    
- Whenever mobile number is eligible to register, either this number is not at all registered in server or this number belongs to some imported business.     
- For a contact number which is not registered as well as not imported, there will not be any pre-data sent from server in response.    
    
For imported business, API will return pre-registered data for imported business like Name, address, mobile number, city, country etc.    
App UI will automatically fills this data in registration from, which user can edit and change before actually submitting registration form     
    
**Case 1: "eligible": false**    
-  these contact is already registered with Conchact. Hence user can NOT proceed to fill and submit registration from. All other input fields will stay disabled.

    
     
     
### Step 2 ###
**Actual Business registration**     

```
#!python
URL - /api/business/register    
```

This will be same as existing business registration API.   

NOTE: *Business cant change Contact No. if it is imported*
   
1. For imported business - app will send updated data (i.e data edited by user) along with same "id" field sent by "checkMobileNo" API. This will allow server to overwrite imported data with new updated data given during registration process.    
2. For business which is totally new to conchact (i.e not imported) - Registration process will be same as we have it now.    
   
**Below is the sample data sent in POST of registration API  - there is no new change in data format**   
   

Business DTO - Its a Multipart Request    

```
#!python

{    
"business" :      
{
    "id": "123as1as2dae5r4ef1sd3f", / null
    "name": "Abcd",
    "address": "Abcd",
    "contactNo": "214536987",
    "description": "Abcd",
    "country": {
        "callingCode": "91",
        "countryCode": "IN",
        "id": null,
        "name": "India"
    },
    "city": {
        "country": " India",
        "id": "55f52c5e7a42987b4e90968f",
        "name": "Pune",
        "placeId": "ChIJARFGZy6_wjsRQ-Oenb9DjYI"
    },
    "categories": [
        {
            "id": null,
            "name": "Deals & Promotions",
            "selected": true
        }
    ],
    "email": "at@best.com",
    "password": "123456",
    "gcmId": "APA91bGSVwM9U_dAPK7OQyPIVWBw_HS1KpyEH4TrvKp-el5_V-FS3YZz9uLDO",
    "apnId": "",
    "lat": "18.5903728",
    "lng": "73.8185433",    
    "showContact" = true/false (checkbox in UI)
}  
}    
```