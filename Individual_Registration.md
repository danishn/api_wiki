URL - /api/individual/register   
    
Individual DTO - Its a Multipart Request    

```
#!python

    
{    
 "individual" : 
  {
   "name" : "dipmala",    
   "email": "iidipmala111@sudosaints.com",     
   "contactNo":"9989876543",     
   "password":"aaaaaa",    
   "city":"pune",     
   "country":"India",    
   "gcmId": "gcm123string"/null,    
   "apnId": "apn123string"/null,    
   "showContact" = true/false (checkbox in UI)
  },   
    
"profilePicture": "[IMAGE]"    
    
}
```
