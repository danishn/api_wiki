URL - /api/uploadChatImage    
    
Method: Multipart/Form-Data    

```  
#!python
Header     
userid = sender's_User_Id    
    
File DATA    
file = [Chat_IMAGE_FILE_with_any_name.jpg]      
    
Response:     
{file: 'uploaded_file_Path'}

- NOTE: use above path in 'imageUrl' key of 'api/sendMessage' API

```   
